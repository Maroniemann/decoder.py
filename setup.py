 
from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='decoder.py',
      version='1.58XB',
      description='decoder.py is a cross-platform module for decoding compressed audio files.',
      long_description=readme(),
      url='http://www.brailleweb.com/',
      author='Dalen Bernaca',
      author_email='dbernaca@gmail.com',
      license='',
      packages=['decoder'],
      install_requires=[
          'mutagen',
      ],
      include_package_data=True,
      zip_safe=False)
